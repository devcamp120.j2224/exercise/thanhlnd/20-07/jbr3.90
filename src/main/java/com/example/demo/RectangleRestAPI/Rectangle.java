package com.example.demo.RectangleRestAPI;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


    public class Rectangle {
        private float width ;
        private float length ;

        

        public float getWidth() {
            return width;
        }

        public void setWidth(float width) {
            this.width = width;
        }

        public float getLength() {
            return length;
        }

        public void setLength(float length) {
            this.length = length;
        }

        public Rectangle() {

        }

        public Rectangle(float width, float length) {
            this.width = width;
            this.length = length;
        }

        public double getArea(){
            return width * length;
        }
        
        public double getPerimeter(){
            return 2.0*(this.width + this.length);
        }

        @Override
        public String toString() {
            return "Rectangle [length=" + length + ", width=" + width + "]";
        }

}
